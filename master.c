#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "master.h"

// Genere une combinatoire a trouver
void initialiser_solution(int solution[NB_COLONNES])
{
   for(int i = 0; i < NB_COLONNES; i++) solution[i] = rand()%(NB_COULEURS) + 1;
}

// Initialise un plateau vierge
void initialiser_plateau (int plateau[NB_LIGNES][COL_PLATEAU])
{
   for(int i = 0; i < NB_LIGNES; i++)
   {
      for(int j = 0; j < NB_COLONNES + 2; j++)
      {
	 plateau[i][j]	= 0;
	 // printf("%d ", plateau[i][j]);
      }
      // printf("\n");
   }

}

// Calcule le nombre de pions bien places et de pions mal places
// etant donne une proposition et la solution
// retourne le resultat dans une combinaison
combinaison compiler_proposition(int proposition[NB_COLONNES], int solution[NB_COLONNES]) 
{
   int lu[NB_COLONNES] = {0};	 // Tableau des positionnements lus
   combinaison c;		 // Combinaison des bpos / mpos
   
   initialiser_combinaison( &c );

   for(int i = 0; i < NB_COLONNES; i++)
   {
      if (proposition[i] == solution[i])
      {
	 // printf("p[%d]: %d -> bpos\n", i, proposition[i]);
	 
	 // Si cette case a deja ete lue une fois alors,
	 // on decremente mpos
	 if (lu[i] == 1)   c.mpos --;
	 else		   lu[i] ++;		   
	 c.bpos ++;
      }
      else if ( estMalPlace (proposition[i], solution, i, lu) )
      {
	 // printf("p[%d]: %d -> mpos\n", i, proposition[i]);
	 c.mpos ++;
      }
   }
   // printf("C: bpos: %d\tmpos: %d\n", c.bpos, c.mpos);

   return c;
}


void saisirJeu(int saisie[NB_COLONNES])
{
   printf("Saisissez la valeur (entre 1 et %d) des %d pions\n", NB_COULEURS, NB_COLONNES);
   for(int i = 0; i < NB_COLONNES; i++) scanf("%d%*c", &saisie[i]);
}

/************************************************************************
 * x:	 Valeur recherchee
 * k:	 Solution de la partie
 * i:	 Indice de la valeur recherchee
 * lu:	 Tableau a 1 si le nb ds la solution a deja ete comptabilise
 * retour: 1 si la valeur est dans la solution, 0 sinon
 * *********************************************************************/
int estMalPlace(int x, int * k, int i, int * lu)
{
   int j	  = 0;
   int estPresent = 0;

   while ( j < NB_COLONNES && !estPresent)
   {
      // Si le nombre a l'indice j n'est pas deja comptabilise on le compare avec x
      if ( lu[j] == 0 )
      {
	 // Si le nombre dans la solution correspond, on le comptabilise
	 // et on met a jour la variable resultat
	 if ( x == k[j] )
	 {
	    lu[j] ++;
	    estPresent = 1;
	 }
      }
      j++;
   }

   return estPresent;
}

void initialiser_combinaison(combinaison * c) { c->bpos = 0; c->mpos = 0; }

/*******************************************************************************
 * Ajoute la saisie passee en parametre au plateau de jeu
 ******************************************************************************/
void ajouterSaisie(int plateau[NB_LIGNES][COL_PLATEAU], int saisie[NB_COLONNES], const combinaison * tentative, int nbEssai)
{
   
   for (int i = 0; i < NB_COLONNES; i++)
      plateau[nbEssai][i] = saisie[i];
   
   plateau[nbEssai][NB_COLONNES]       = tentative->bpos;
   plateau[nbEssai][NB_COLONNES + 1]   = tentative->mpos;
}

void afficher_plateau(int plateau[NB_LIGNES][COL_PLATEAU])
{
   // Afficher le dessus
   puts("-------------b-m--");

   // Affichage des lignes
   for( int i = 0;  i < NB_LIGNES; i++ )
   {
      printf("| ");

      // Afficher une combinaisons
      for (int j = 0; j < NB_COLONNES; j++) printf ("%d ", plateau[i][j]);
      printf("|| ");

      // Afficher les bpos / mpos
      printf("%d %d |\n", plateau[i][NB_COLONNES], plateau[i][NB_COLONNES + 1]);
   }

   // Afficher la fin
   puts("------------------");
}
