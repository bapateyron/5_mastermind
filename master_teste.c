#include "master.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char * argv[])
{
   if (argc > 1)  srand( atoi(argv[1]) );
   else		  srand( time(NULL) );
   
   int	       nbEssai	   = 0;
   combinaison tentative;

   // Declaration
   int saisie[NB_COLONNES]		     = {0};
   int solution[NB_COLONNES]		     = {0};
   int plateau[NB_LIGNES][NB_COLONNES + 2]   = {0};

   // Initialisation
   initialiser_solution( solution );
   initialiser_plateau( plateau );
   initialiser_combinaison( &tentative );


   // Jeu
   while( tentative.bpos != 4 && nbEssai < NB_LIGNES )
   {
      saisirJeu(saisie);

      tentative = compiler_proposition(saisie, solution);

      ajouterSaisie( plateau, saisie, &tentative, nbEssai++ );

      // Affichage
      afficher_plateau(plateau);

      // Solution //
      printf("# %d %d %d %d ########\n", solution[0], solution[1], solution[2], solution[3]);
   }

   if ( tentative.bpos == 4)  puts("Bravo, vous avez gagné !");
   else			      puts("Perdu !");

   return 0;
}
