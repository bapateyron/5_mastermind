#ifndef master_h
#define master_h

/* DECLARATION DES CONSTANTES SYMBOLIQUES */

#define NB_LIGNES    10
#define NB_COLONNES  4
#define COL_PLATEAU  NB_COLONNES + 2
#define NB_COULEURS  6

/* DECLARATION DES TYPES PERSONNELS */
// et de leur typedef si besoin

typedef struct {
   int bpos;
   int mpos;
} combinaison;

/* DECLARATIONS DES METHODES */

void initialiser_solution(int solution[NB_COLONNES]);
void initialiser_plateau (int plateau[NB_LIGNES][COL_PLATEAU]);
combinaison compiler_proposition(int proposition[NB_COLONNES], int solution[NB_COLONNES]); 

// mettre ici les autres declarations
void saisirJeu(int saisie[NB_COLONNES]);

/************************************************************************
 * x:	 Valeur recherchee
 * k:	 Solution de la partie
 * i:	 Indice de la valeur recherchee
 * lu:	 Tableau a 1 si le nb ds la solution a deja ete comptabilise
 * retour: 1 si la valeur est dans la solution, 0 sinon
 * *********************************************************************/
int estMalPlace(int x, int * k, int i, int * lu);
void initialiser_combinaison(combinaison * c);
void ajouterSaisie(int plateau[NB_LIGNES][COL_PLATEAU], int saisie[NB_COLONNES], const combinaison * tentative, int nbEssai);
void afficher_plateau(int plateau[NB_LIGNES][COL_PLATEAU]);

#endif
